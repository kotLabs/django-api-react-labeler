FROM python:3.7-alpine

# Ensure that Python outputs everything that's printed inside
# the application rather than buffering it.
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/app/web

COPY requirements.txt ./

# virtual deps for building psycopg2 and Pillow
RUN apk add --no-cache --virtual .build-deps \
        postgresql-dev \
        gcc \
        python3-dev \
        musl-dev \
        jpeg-dev \
        zlib-dev \
    && apk add libjpeg \
    && pip install -r requirements.txt \
    && apk del .build-deps

ENV DJANGO_ENV=dev

EXPOSE 8000

COPY . .

ENTRYPOINT [ "sh", "./entrypoint.sh"]