from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from .models import CompanyDetails, Products, Batches


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class ProductsSerializer(serializers.ModelSerializer):

    def validate_company(self, value):
        user = self.context['request'].user
        if value.id not in user.created_companies.values_list('id', flat=True):
            raise serializers.ValidationError('Invalid company ID')
        return value

    class Meta:
        model = Products
        fields = ['id', 'name_lt', 'name_en', 'is_organic', 'company']


# Serializer to show less fields of Production in nested Batches GET requests
class BatchesProductsSerializer(ProductsSerializer):
    class Meta:
        model = Products
        fields = ['name_lt', 'name_en', 'is_organic']


class BatchesSerializer(serializers.ModelSerializer):
    production_info = serializers.SerializerMethodField(read_only=True)

    def get_production_info(self, obj):
        production = obj.production
        serializer = BatchesProductsSerializer(production)
        return serializer.data

    def validate_company(self, value):
        user = self.context['request'].user
        valid_companies = user.created_companies.values_list('id', flat=True)
        if value.id not in valid_companies:
            raise serializers.ValidationError('Invalid company ID')
        return value

    def validate_production(self, value):
        company_id = int(self.context['request'].data['company'])
        valid_products = Products.objects.filter(company=company_id).values_list('id', flat=True)

        if value.id not in valid_products:
            raise serializers.ValidationError('Invalid production ID')
        return value

    class Meta:
        model = Batches
        fields = ['id', 'batch_no', 'batch_date', 'production', 'production_info', 'company']
        validators = [
            UniqueTogetherValidator(
                queryset=Batches.objects.all(),
                fields=['batch_no', 'company'],
                message='Batch with this number already exists'
            )
        ]


class CompanyDetailsSerializer(serializers.ModelSerializer):
    creator = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        model = CompanyDetails
        fields = ['id', 'name', 'reg_code', 'address', 'company_logo',
                  'organic_logo', 'cert_body_code', 'cert_body_country', 'creator']
