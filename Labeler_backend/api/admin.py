from django.contrib import admin

from .models import CompanyDetails, Products, Batches

admin.site.register(CompanyDetails)
admin.site.register(Products)
admin.site.register(Batches)