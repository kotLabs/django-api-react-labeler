from django.db import models
from django.contrib.auth.models import User


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.creator.id, filename)


# TODO make ono-to-one for user
# Label fields, that will apply to all generated labels by that user
class CompanyDetails(models.Model):
    creator = models.ForeignKey(User,
                                on_delete=models.DO_NOTHING,
                                related_name='created_companies')
    name = models.CharField(max_length=50)
    reg_code = models.CharField(max_length=20, blank=True)
    address = models.CharField(max_length=100, blank=True)
    company_logo = models.ImageField(upload_to=user_directory_path, blank=True)
    organic_logo = models.ImageField(upload_to=user_directory_path, blank=True)
    cert_body_code = models.CharField(max_length=10, blank=True)
    cert_body_country = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return self.name


class Products(models.Model):
    name_lt = models.CharField(max_length=50)
    name_en = models.CharField(max_length=50, blank=True)
    is_organic = models.BooleanField(default=True)
    company = models.ForeignKey(CompanyDetails,
                                on_delete=models.CASCADE,
                                blank=False)

    class Meta:
        ordering = ['company', '-is_organic', 'name_lt']

    def __str__(self):
        return "{}/{}".format(self.name_lt, self.name_en)


class Batches(models.Model):
    batch_no = models.CharField(max_length=20)
    batch_date = models.DateField()
    production = models.ForeignKey(Products,
                                   on_delete=models.PROTECT,
                                   related_name='batches')
    company = models.ForeignKey(CompanyDetails,
                                on_delete=models.CASCADE,
                                related_name='batches',
                                blank=False)

    class Meta:
        ordering = ['-batch_date', '-batch_no']

    def __str__(self):
        return "{} {} {}".format(self.batch_date, self.batch_no, self.production.name_lt)
