from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError
from rest_framework.viewsets import ModelViewSet
from .serializers import UserSerializer, GroupSerializer
from .permissions import IsInCompany
from .serializers import CompanyDetailsSerializer, ProductsSerializer, BatchesSerializer
from .models import CompanyDetails, Products, Batches


class UserViewSet(ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class CompanyDetailsViewSet(ModelViewSet):
    serializer_class = CompanyDetailsSerializer
    permission_classes = [IsInCompany]

    # Only companies created by logged in user will be returned
    def get_queryset(self):
        user = self.request.user
        return CompanyDetails.objects.filter(creator=user)


class ProductsViewSet(ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductsSerializer
    permission_classes = [IsInCompany]

    def get_queryset(self):
        super().get_queryset()

        # Only products of related companies are returned
        user = self.request.user
        queryset = Products.objects.filter(company__creator=user)

        # Filtering queryset by company id, if provided
        company = self.request.query_params.get('company', None)
        if company is not None:
            queryset = queryset.filter(company=company)

        return queryset


class BatchesViewSet(ModelViewSet):
    queryset = Batches.objects.all()
    serializer_class = BatchesSerializer
    permission_classes = [IsInCompany]

    def get_queryset(self):
        super().get_queryset()

        # Only batches of companies created by logged in user will be returned
        user = self.request.user
        queryset = Batches.objects.filter(company__creator=user)

        # Filtering queryset by company id, if provided
        company = self.request.query_params.get('company', None)
        if company is not None:
            queryset = queryset.filter(company=company)

        # Filtering queryset by date_from
        # Invalid inputs will be ignored
        date_from = self.request.query_params.get('date_from', None)
        if date_from is not None:
            try:
                queryset = queryset.filter(batch_date__gte=date_from)
            except ValidationError:
                pass

        # Filtering queryset by date_to
        # Invalid inputs will be ignored
        date_to = self.request.query_params.get('date_to', None)
        if date_to is not None:
            try:
                queryset = queryset.filter(batch_date__lte=date_to)
            except ValidationError:
                pass

        return queryset
