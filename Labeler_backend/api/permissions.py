from rest_framework.permissions import IsAuthenticated


class IsInCompany(IsAuthenticated):
    """
    Allows access only if user is creator of company or has
    permissions.

    This permission applies for objects with 'creator' attribute
    or object with 'company' attribute
    """

    # TODO permit other allowed users, when functionality for other
    #  allowed users will be added
    def has_object_permission(self, request, view, obj):
        user = request.user
        if hasattr(obj, 'creator'):
            return user == obj.creator
        if hasattr(obj, 'company'):
            return user == obj.company.creator
        return False
