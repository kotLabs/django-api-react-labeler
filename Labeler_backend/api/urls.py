from django.urls import path, include
from rest_framework import routers
from django.conf.urls import url

from . import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'company', views.CompanyDetailsViewSet, 'company')
router.register(r'products', views.ProductsViewSet, 'products')
router.register(r'batches', views.BatchesViewSet, 'batches')

urlpatterns = [
    path('', include(router.urls)),
    url(r'^auth/', include('djoser.urls')),
    url(r'^auth/', include('djoser.urls.authtoken')),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]