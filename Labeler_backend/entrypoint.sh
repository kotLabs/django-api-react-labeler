#!/bin/bash
python manage.py migrate

echo Starting Gunicorn
exec gunicorn Labeler_backend.wsgi -b 0.0.0.0:8000 --workers 3