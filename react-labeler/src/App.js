import React, { Component } from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css';
import MenuBar from './components/MenuBar';
import { withCookies } from "react-cookie";
import {Container, Grid} from "semantic-ui-react";
import API from "./services/Api";
import MainBody from "./components/MainBody";


class App extends Component {


    state = {
        activeMenuItem: 'batches',
        products: [],
        companies: [],
        token: this.props.cookies.get('lab-token'),
        user: '',
        selectedCompany: null // TODO use cookies to save selected company id
    };

    async componentDidMount() {
        // TODO consider moving token check to render() or other place, which runs before first render()
        if(this.state.token) {
            // Get current user
            let userData = await API.getCurrentUser(this.state.token);
            this.setState({user: userData});

            // Get company data
            let companyData = await API.getCompanyData(this.state.token);
            this.setState({companies: companyData});
            if (this.state.companies.length > 0) {
                // selectedCompany will be the first one in the array (created first)
                this.setState({selectedCompany: this.state.companies[0]})
            } else {
                this.setState({
                    selectedCompany: null,
                    activeMenuItem: 'company'
                })
            }

            // Get products data
            let products = await API.getProducts(this.state.token);
            this.setState({products: products});


        } else {
            // if user is not logged in (no token), kick to login
            window.location.href = '/';
        }

    }
    // TODO Move to API
    logout = () => {
        fetch(`${process.env.REACT_APP_API_URL}/auth/token/logout/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${this.state.token}`
            }
        })
            .then(response => {
                this.setState({
                    token: null
                });
                window.location.href = '';
            })
            .catch(error => console.log(error))
    };

    setActiveMenuItem = (item) => {
      this.setState({activeMenuItem: item})
    };

    addNewProduct = (product) => {
        this.setState({
            products: [product, ...this.state.products]
        })
    };

    addNewCompany = (company) => {
        this.setState({
            companies: [company, ...this.state.companies]
        })
    };

    updateCompanyInfo = (company) => {
        const companiesClone = [...this.state.companies];
        companiesClone.forEach((item, index, arr) => item.id === company.id ? arr[index] = company : '');
        this.setState({
            companies: companiesClone
        })
    }

    selectCompany = (company) => {
        this.setState({
            selectedCompany: company
        })
    };

    deleteCompany = (company) => {
        API.deleteCompany(this.state.token, company);
        // TODO check needed if status 204 or delete ok
        const deletedCompany = company;
        const companies = this.state.companies.filter(company => company.id !== deletedCompany.id);
        this.setState({
            companies: companies,
            selectedCompany: null
        })
    };

    deleteProduct = (product) => {
        API.deleteProduct(this.state.token, product);
        // TODO check needed if status 204 or delete ok
        // TODO handle cases, when product is referred and deletion is not allowed
        const deletedProduct = product;
        const products = this.state.products.filter(product => product.id !== deletedProduct.id);
        this.setState({
            products: products
        })
    };

    render() {
        return (
            // <Container>
            <Grid>
                <Grid.Row>
                    <MenuBar
                        logout={this.logout}
                        username={this.state.user.username}
                        activeMenuItem={this.state.activeMenuItem}
                        setActiveMenuItem={this.setActiveMenuItem}
                        selectedCompany={this.state.selectedCompany}
                    />
                </Grid.Row>
                <Grid.Row>
                    <Container>
                    <MainBody
                        token={this.state.token}
                        products={this.state.products}
                        addNewProduct={this.addNewProduct}
                        deleteProduct={this.deleteProduct}
                        tab={this.state.activeMenuItem}
                        companies={this.state.companies}
                        addNewCompany={this.addNewCompany}
                        selectedCompany={this.state.selectedCompany}
                        selectCompany={this.selectCompany}
                        deleteCompany={this.deleteCompany}
                        updateCompanyInfo={this.updateCompanyInfo}

                    />
                    </Container>

                </Grid.Row>
            </Grid>
            // </Container>
      );
    }
}

export default withCookies(App);
