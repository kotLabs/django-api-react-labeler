class Api {

    static tokenLogin(credentials) {
        return (fetch(`${process.env.REACT_APP_API_URL}/auth/token/login/`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify(credentials)
        })).then( r => r.json())
            .then( data => ({
                token: data.auth_token
            }))
            .catch( error => ({
                error: error
            }))
    };

    static register(credentials) {
        return (
            fetch(`${process.env.REACT_APP_API_URL}/auth/users/`, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json'},
                body: JSON.stringify(credentials)
            }).then( r => r.json())
                .then( data => ({ok: true}))
                .catch( error => console.log(error)))
        };

    static getCurrentUser(token) {
        return (
            fetch(`${process.env.REACT_APP_API_URL}/auth/users/me/`, {
                method: 'GET',
                headers: {
                    'Authorization': `Token ${token}`
                }
            }).then(r=> r.json())
                .catch(error=> console.log(error))
        )
    }

     static getItems(token, uri) {
        return (
            fetch(`${process.env.REACT_APP_API_URL}/${uri}/`, {
                method: 'GET',
                headers: {
                    'Authorization': `Token ${token}`
                }
            }).then(r=> r.json())
                .catch(error=> console.log(error))
        )
    }

    static getBatches(token) {
        return this.getItems(token, 'batches')
    }

    static getProducts(token) {
        return this.getItems(token, 'products')
    }

    static getCompanyData(token) {
        return this.getItems(token, 'company')
    }

    static postItem(token, uri, item) {
        return (
            fetch(`${process.env.REACT_APP_API_URL}/${uri}/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(item)
            }).then(r=> {
                if (!r.ok) {
                    throw new Error("Error, status = " + r.status);
                }
                return r.json()
            })
                .catch(error=> console.log(error))
        )
    }

    static postProducts(token, product) {
        return this.postItem(token, 'products', product)
    }

    static postBatches(token, batch) {
        return this.postItem(token, 'batches', batch)
    }

    static postCompany(token, companyFormData) {
        return (
            fetch(`${process.env.REACT_APP_API_URL}/company/`, {
                method: 'POST',
                headers: {
                    'Authorization': `Token ${token}`
                },
                body: companyFormData
            }).then(r=> {
                if (!r.ok) {
                    throw new Error("Error, status = " + r.status);
                }
                return r.json()
            })
                .catch(error=> console.log(error))
        )
    }

    static putCompany(token, companyID, companyFormData) {
        return (
            fetch(`${process.env.REACT_APP_API_URL}/company/${companyID}/`, {
                method: 'PUT',
                headers: {
                    'Authorization': `Token ${token}`
                },
                body: companyFormData
            }).then(r=> {
                if (!r.ok) {
                    throw new Error("Error, status = " + r.status);
                }
                return r.json()
            })
                .catch(error=> console.log(error))
        )
    }


    static deleteItem(token, uri) {
        fetch(`${process.env.REACT_APP_API_URL}/${uri}/`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${token}`
            }
        })
            .then(r =>  {
                if (r.status === 204) {
                    return r;
                }
            })
            .catch(error => console.log(error))
    };

    static deleteProduct(token, product) {
        const uri = `products/${product.id}`;
        return this.deleteItem(token, uri)
    }

    static deleteBatch(token, batch) {
        const uri = `batches/${batch.id}`;
        return this.deleteItem(token, uri)
    };

    static deleteCompany(token, company) {
        const uri = `company/${company.id}`;
        return this.deleteItem(token, uri)
    }

}

export default Api;