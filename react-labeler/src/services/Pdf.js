import {PDFDownloadLink,
    StyleSheet,
    Document,
    Page,
    Text,
    View,
    Font,
    Image}
    from '@react-pdf/renderer'
import React from "react";
import palemonas from './fonts/Palemonas_3_2/Palem3.2-nm.ttf';
import palemonas_bd from './fonts/Palemonas_3_2/Palem3.2-bd.ttf';
import {Button, Icon} from "semantic-ui-react";

function GeneratePDF(props) {
    const filename = props.batch.batch_no + '.pdf'
    Font.register({
        family: 'Palemonas',
        fonts: [
            {src: palemonas},
            {src: palemonas_bd, fontWeight: 700}
        ]
    });
    const styles = StyleSheet.create({
        sheet: {
            flexDirection: 'row',
            flexWrap: 'wrap'
        },
        label: {
            flexDirection: 'column',
            justifyContent: 'space-between',
            width: '105mm',
            fontFamily: 'Palemonas',
            height: '99mm',
            padding: '9mm',
            fontSize: '13pt',
            border: '1 solid black'
        },
        companyName: {
            fontSize: '14pt',
            fontWeight: 'bold',
            textAlign: 'center',
        },
        companyDetails: {
            fontSize: '9pt',
            textAlign: 'center'
        },
        productionName: {
            fontSize: '16pt',
            fontWeight: 'bold',
            marginLeft: '10pt',
            padding: '5',
        },
        batchDetails: {
            fontSize: '12pt',
        },
        logos: {
            flexDirection: 'row',
            alignContent: 'space-evenly',
            justifyContent: 'center',
            alignItems: 'center',
            height: '20mm'
        },
        logoOrganic: {
            // flexDirection: 'column',
            maxWidth: '30mm',
            maxHeight: '20mm',
            height: 'auto',
            width: 'auto',
            objectFit: 'contain'
        },
        logoCompany: {
            // flexDirection: 'column',
            maxWidth: '53mm',
            objectFit: 'contain',
            // maxHeight: '16mm',
            height: 'auto',
            width: 'auto'
        },
        certBodyInfo: {
            fontSize: '12pt'
        }
    });

    const BatchLabel = () => (
        <View style={styles.label}>
            <Text style={styles.companyName}>
                {props.company.name}
            </Text>
            <Text style={styles.companyDetails}>
                {props.company.reg_code} {props.company.address}
            </Text>
            <View style={styles.productionName}>
                <Text>
                    {props.batch.production_info.name_lt}
                </Text>
                <Text>
                    {props.batch.production_info.name_en}
                </Text>
            </View>
            <View style={styles.batchDetails}>
                <Text>
                    Partija/Lot.No. {props.batch.batch_no}
                </Text>
                <Text>
                    Data/Date: {props.batch.batch_date}
                </Text>
            </View>
            <View style={styles.logos}>
                {props.batch.production_info.is_organic &&
                    <Image style={styles.logoOrganic} src={props.company.organic_logo}/>
                }
                {props.company.company_logo ?
                    <Image style={styles.logoCompany} src={props.company.company_logo}/>
                    : null
                }
            </View>
            <View style={styles.certBodyInfo}>
                <Text>{props.company.cert_body_code}</Text>
                <Text>{props.company.cert_body_country}</Text>
            </View>
        </View>
    );

    const BatchSheet = () => (
        <Document>
            <Page size="A4">
                <View style={styles.sheet}>
                    <BatchLabel/>
                    <BatchLabel/>
                    <BatchLabel/>
                    <BatchLabel/>
                    <BatchLabel/>
                    <BatchLabel/>
                </View>
            </Page>
        </Document>
    );

    return (
        <div>
            <PDFDownloadLink document={<BatchSheet/>} fileName={filename}>
                {({blob, url, loading, error}) => (
                    loading ?
                        <Button disabled>Generating document...</Button>
                        :
                        <Button
                            icon
                            color='green'
                            labelPosition='left'
                        >{filename} <Icon name='download'/></Button>
                )}
            </PDFDownloadLink>
        </div>
    )
}

export default GeneratePDF;