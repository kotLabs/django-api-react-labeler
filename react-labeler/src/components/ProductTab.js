import React from "react";
import {Button, Container, Header, Icon, List, Form} from "semantic-ui-react";
import Api from "../services/Api"


class ProductTab extends React.Component {

    constructor(props) {
        super(props);

        this.clearProduct = {
            name_lt: '',
            name_en: '',
            is_organic: true,
            company: this.props.selectedCompany.id
        };

        this.state = {
            isFormHidden: true,
            new_product: {...this.clearProduct}
        };
        this.handleAddProduct = this.handleAddProduct.bind(this)
    }

    newProductClicked = () => {
        this.setState((state) => ({isFormHidden: !state.isFormHidden}))
    };

    handleChangeNewProd = (e, {name, value}) => {
        let new_prod = this.state.new_product;
        new_prod[name] = value;
        this.setState({new_product: new_prod})
    };

    handleCheckboxChange = () => {
        let new_prod = this.state.new_product;
        new_prod['is_organic'] = !this.state.new_product.is_organic;
        this.setState({new_product: new_prod})
    };

    cancelForm = () => {
        this.setState({
            isFormHidden: true,
            new_product: {...this.clearProduct}});
    };

    async handleAddProduct() {
        const response = await Api.postProducts(this.props.token, this.state.new_product);
        if (response) {                       // If product was successfully created
            this.props.addNewProduct(response);  // add it to state
            this.setState({
                isFormHidden: true,
                new_product: {...this.clearProduct}
            })
        }
    };

    handleDelete = (product) => {
        this.props.deleteProduct(product)
    };

    render() {
        const isFormHidden = this.state.isFormHidden;

        return (
            <Container className='production-tab'>
                <Header as='h3'>Production List</Header>
                <Button onClick={this.newProductClicked}>
                            <Icon name='plus'/> New product..
                </Button>

                <Container>
                    <Form
                        hidden={isFormHidden}
                        className='fluid segment'>
                      <Form.Input
                          label='Product name'
                          placeholder='Product name'
                          type='text'
                          required
                          name='name_lt'
                          value={this.state.new_product.name_lt}
                          onChange={this.handleChangeNewProd}
                      />
                      <Form.Input
                          label='Product name (in other language)'
                          placeholder='Product name'
                          type='text'
                          name='name_en'
                          value={this.state.new_product.name_en}
                          onChange={this.handleChangeNewProd}
                      />
                      <Form.Checkbox
                          inline
                          toggle
                          label='Organic'
                          checked={this.state.new_product.is_organic}
                          onChange={this.handleCheckboxChange}
                      />
                      <Button color='blue' onClick={this.handleAddProduct}>Add</Button>
                        <Button color='red' onClick={this.cancelForm}>Cancel</Button>
                    </Form>
                </Container>

                {/*TODO Create decently looking component for list items*/}
                {/*TODO Add edit functionality*/}
                <List relaxed divided ordered>
                    {this.props.products.map(product => {
                        return (
                            <List.Item key={product.id}>
                                <List.Content>
                                    <List.Header>
                                        {product.name_lt}
                                        <Icon
                                            bordered
                                            link
                                            color='red'
                                            name='trash'
                                            onClick={() => this.handleDelete(product)}
                                        />
                                    </List.Header>
                                    <List.Description>
                                        {product.name_en}
                                    </List.Description>
                                </List.Content>
                            </List.Item>
                        )
                    })}
                </List>
            </Container>

        )
    }
}

export default ProductTab;