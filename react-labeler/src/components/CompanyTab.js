import React from "react";
import CompanyDetails from "./CompanyDetails";
import {Button, Container, Dropdown, Icon} from "semantic-ui-react";
import CompanyForm from "./CompanyForm";

class CompanyTab extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editedCompany: null,
            // TODO IMPORTANT! remove this derived state, if unnecessary
            // probably this causes not switching to newly created company after creation
            selectedCompany: this.props.selectedCompany
        }
    }

    createNew = () => {
        this.setState({editedCompany: {
                name: '',
                reg_code: '',
                address: '',
                company_logo: '',
                organic_logo: '',
                cert_body_code: '',
                cert_body_country: ''}})
    };

    handleRemove = (company) => {
        this.props.deleteCompany(company)
        this.setState({selectedCompany: {id: null}})
    };

    editCompany = (company) => {
        this.setState({editedCompany: company})
    };

    cancelForm = () => {
        this.setState({editedCompany: null})
    };

    handleDropdown =(e, {value}) => {
        const companyID = value;
        const company = this.props.companies.find((value) => value.id === companyID);
        this.props.selectCompany(company);
        this.setState({selectedCompany: company})
    };

    render() {
        return (
            <Container>
                <Button onClick={this.createNew}>
                        <Icon name='plus'/> Create new..
                </Button>
                {this.state.selectedCompany &&
                    <Dropdown
                        name='selectedCompany'
                        value={this.state.selectedCompany.id}
                        onChange={this.handleDropdown}
                        button
                        text='Switch company'
                        openOnFocus
                        options={
                            this.props.companies.map( company => {
                                return (
                                    {key: company.id, text: company.name, value: company.id}
                                )
                            })
                        }
                    />
                }
                {!this.state.editedCompany ? (
                    this.state.selectedCompany &&
                        <CompanyDetails
                            companies={this.props.companies}
                            selectedCompany={this.state.selectedCompany}
                            createNew={this.createNew}
                            editCompany={this.editCompany}
                            handleRemove={this.handleRemove}
                        />
                ) :
                    <CompanyForm
                        company={this.state.editedCompany}
                        addNewCompany={this.props.addNewCompany}
                        selectCompany={this.props.selectCompany}
                        cancelForm={this.cancelForm}
                        token={this.props.token}
                        updateCompanyInfo={this.props.updateCompanyInfo}
                    />
                }

            </Container>
        )
    }
}

export default CompanyTab;