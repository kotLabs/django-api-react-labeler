import React, { Component } from "react";
import { withCookies} from "react-cookie";
import {Grid, Header, Segment, Button, Form, Message} from "semantic-ui-react";
import Api from "../services/Api"

// TODO add retype password
// TODO add message fields for login errors

class Login extends Component {
    constructor() {
        super();
        this.state = {
            credentials: {
                username: '',
                password: ''
            },
            isLoginView: true
        };
        this.login = this.login.bind(this);
        this.register = this.register.bind(this);

    }

    handleChange = (e, { value, name }) => {
        let cred = this.state.credentials;
        cred[name] = value;
        this.setState({ credentials: cred});
    };

    async login() {
        const response = await Api.tokenLogin(this.state.credentials);
        if (response.token) {
            this.props.cookies.set('lab-token', response.token)
            window.location.href = "./labels";
        } else {
            // failed to log in TODO add message about failed log in
            console.log('Login failed')
            if (response) {
                console.log(response)
            }
        }
    };

    async register() {
        const response = await Api.register(this.state.credentials);
        if (response.success) {
            this.setState({isLoginView: true});
        } else { // TODO add better error handling
            console.log('provided credentials: ' + this.state.credentials)
        }
    };

    toggleView = () => {
      this.setState({isLoginView: !this.state.isLoginView})
    };

    render() {
        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                <Grid.Column style={{ maxWidth: 450 }}>
                  <Header as='h2' color='green' textAlign='center'>
                      { this.state.isLoginView ? 'Log-in to your account' : 'Register new account' }
                  </Header>
                  <Form size='large'>
                    <Segment stacked>
                      <Form.Input
                          fluid icon='user' iconPosition='left'
                          placeholder='Username'
                          value={this.state.credentials.username}
                          name='username'
                          onChange={this.handleChange}
                      />
                      <Form.Input
                        fluid
                        icon='lock'
                        iconPosition='left'
                        placeholder='Password'
                        type='password'
                        value={this.state.credentials.password}
                        name='password'
                        onChange={this.handleChange}
                      />

                        {this.state.isLoginView ? (
                            <Button color='green' fluid size='large' onClick={this.login}>
                                Login
                            </Button>
                        ) : (
                            <Button color='green' fluid size='large' onClick={this.register}>
                                Register
                            </Button>
                        )}
                    </Segment>
                  </Form>
                  {this.state.isLoginView ? (
                      <Message>
                        New to us? <br/>
                          <Button fluid onClick={this.toggleView}>Sign Up</Button>
                      </Message>
                  ) : (
                      <Message>
                          Already have account? <br/>
                          <Button fluid onClick={this.toggleView}>Go to Login</Button>
                      </Message>
                  )}
                </Grid.Column>
              </Grid>
        )
    }
}

export default withCookies(Login);