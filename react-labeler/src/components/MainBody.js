import React from "react";
import BatchTab from "./BatchTab";
import ProductTab from "./ProductTab";
import CompanyTab from "./CompanyTab";

class MainBody extends React.Component {

    render() {
        const tab = this.props.tab;
        let finalComponent;

        const productsFiltered = this.props.products.filter(
            (value, index, array) => {
                return value.company === this.props.selectedCompany.id
            }
        );

        if (tab === 'batches') {
            finalComponent = <BatchTab
                token={this.props.token}
                products={productsFiltered}
                selectedCompany={this.props.selectedCompany}
                />
        } else if (tab === 'products') {
            finalComponent = <ProductTab
                products={productsFiltered}
                addNewProduct={this.props.addNewProduct}
                deleteProduct={this.props.deleteProduct}
                token={this.props.token}
                selectedCompany={this.props.selectedCompany}
            />
        } else if (tab === 'company') {
            finalComponent = <CompanyTab
                token={this.props.token}
                companies={this.props.companies}
                addNewCompany={this.props.addNewCompany}
                selectedCompany={this.props.selectedCompany}
                selectCompany={this.props.selectCompany}
                deleteCompany={this.props.deleteCompany}
                updateCompanyInfo={this.props.updateCompanyInfo}
            />
        }

        return (
            <div>{finalComponent}</div>
        )
    }

}

export default MainBody;