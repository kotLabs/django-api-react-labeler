import React from "react";
import {Button, Header, Menu, Icon} from "semantic-ui-react";

class MenuBar extends React.Component {

    logoutUser = () => {
        this.props.logout()
    };

    handleMenuItemClick = (e, {name}) => {
        this.props.setActiveMenuItem(name)
    };

    render() {
        const activeItem = this.props.activeMenuItem;

        return (
            <Menu fluid size='large' inverted color='green'>
                <Menu.Item><Header as='h1' color='grey'>Labeler</Header></Menu.Item>
                {this.props.selectedCompany &&
                    <Menu.Item
                        name={'batches'}
                        active={activeItem === 'batches'}
                        onClick={this.handleMenuItemClick}>
                        Batches
                    </Menu.Item>
                }
                {this.props.selectedCompany &&
                    <Menu.Item
                        name={'products'}
                        active={activeItem === 'products'}
                        onClick={this.handleMenuItemClick}>
                        Products
                    </Menu.Item>
                }
                <Menu.Item
                    name={'company'}
                    active={activeItem === 'company'}
                    onClick={this.handleMenuItemClick}>
                    Company
                </Menu.Item>
                <Menu.Item position='right'>Logged in as {this.username}</Menu.Item>
                <Menu.Item disabled={true}><Icon name='user'/> {this.props.username}</Menu.Item>
                <Menu.Item><Button onClick={this.logoutUser}>Logout</Button></Menu.Item>
            </Menu>
        )
    }

}
export default MenuBar;