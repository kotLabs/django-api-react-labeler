import React from "react";
import {Button, Grid, Header, Icon} from "semantic-ui-react";
import BatchList from "./BatchList";
import BatchDetails from "./BatchDetails";
import BatchForm from "./BatchForm";
import API from "../services/Api";

class BatchTab extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedBatch: null,
            editedBatch: null,
            batches: [],
        };

        this.deleteBatch = this.deleteBatch.bind(this)
    }

     componentDidMount() {
        if (this.props.selectedCompany) {
            this.loadBatches()
        }
     }

     componentDidUpdate(prevProps) {
        if (this.props.selectedCompany !== prevProps.selectedCompany) {
            this.loadBatches();
        }
     }


    async loadBatches() {
        // Get batches data
         let batches = await API.getBatches(this.props.token);
         const batchesFiltered = batches.filter(
             (value, index, array) => {
                 return value.company === this.props.selectedCompany.id
             }
         )
         this.setState({batches: batchesFiltered});
     };

    showBatch = batch => {
        (
            this.state.selectedBatch === batch ? (
                this.setState({selectedBatch: null}))
            : (
                this.setState({selectedBatch: batch, editedBatch: null}))
        )
    };

    newBatch = () => {
        this.setState({
            editedBatch: {
                batch_no: '',
                batch_date: '',
                company: this.props.selectedCompany.id,
                production: ''
        }})
    };

    editBatch = (batch) => {
        this.setState({editedBatch: batch})
    };

    cloneBatch = (batch) => {
        this.setState({
            editedBatch: {
                batch_no: batch.batch_no,
                batch_date: batch.batch_date,
                production: batch.production,
                company: batch.company
        }})
    };

    cancelForm = () => {
        this.setState({editedBatch: null})
    };

    // updates states after PUT
    batchUpdated = (batch) => {
        const batchesClone = [...this.state.batches];
        batchesClone.forEach((item, index, arr) => item.id === batch.id ? arr[index] = batch : '');
        this.setState({
            batches: batchesClone,
            editedBatch: null,
            selectedBatch: batch
        })
    };

    addBatch = (batch) => {
        this.setState({
            batches: [batch, ...this.state.batches],
            editedBatch: null,
            selectedBatch: batch
        });
    };

    async deleteBatch(batch) {

        await API.deleteBatch(this.props.token, batch);
        const removedBatch = batch;
        const batches = this.state.batches.filter(batch => batch.id !== removedBatch.id);
        this.setState({
            batches: batches,
            selectedBatch: null
        })

    };

    render() {
        return (
            <Grid centered container divided columns={2}>
            {/*     Left side panel*/}
            <Grid.Column>
                <Grid.Row>
                    <Header as='h3'>Batches</Header>

                    <Button onClick={this.newBatch}>
                        <Icon name='plus'/> New batch..
                    </Button>

                    <BatchList batches={this.state.batches} batchClicked={this.showBatch}/>
                </Grid.Row>
            </Grid.Column>
            {/*    Right side panel*/}
            <Grid.Column>
                <Grid.Row>
                    {!this.state.editedBatch ? (
                        <BatchDetails
                            batch={this.state.selectedBatch}
                            editBatch={this.editBatch}
                            deleteBatch={this.deleteBatch}
                            cloneBatch={this.cloneBatch}
                            company={this.props.selectedCompany}
                        />
                    ) :
                        <BatchForm
                            batch={this.state.editedBatch}
                            products={this.props.products}
                            cancelForm={this.cancelForm}
                            addBatch={this.addBatch}
                            showBatch={this.showBatch}
                            batchUpdated={this.batchUpdated}
                            token={this.props.token}
                        />
                    }
                </Grid.Row>
            </Grid.Column>
        </Grid>
        )

    }
}

export default BatchTab;