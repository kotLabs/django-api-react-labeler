import React from "react";
import {List} from 'semantic-ui-react'

function BatchList(props) {

    const batchClicked = batch => evt => {
        props.batchClicked(batch);
    };


    return (
        <List divided relaxed>
            {props.batches.map(batch => {
                return (
                    <List.Item key={batch.id} onClick={batchClicked(batch)}>
                        <List.Content>
                            <List.Header>
                                {batch.batch_no}
                            </List.Header>
                            <List.Description>
                                {batch.batch_date} |
                                {batch.production_info.name_lt}
                            </List.Description>
                        </List.Content>
                    </List.Item>
                )
            })}
        </List>
    )
}

export default BatchList;