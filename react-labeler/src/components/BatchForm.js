import React, { Component} from "react";
import { Dropdown, Button, Form } from 'semantic-ui-react';
import Api from "../services/Api";


class BatchForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            editedBatch: this.props.batch
        };

        this.createNewClicked = this.createNewClicked.bind(this);

    }

    cancelClicked = () => {
        this.props.cancelForm();
    };

    handleChange = (e, { value, name }) => {
        let batch = this.state.editedBatch;
        batch[name] = value;
        this.setState({ editedBatch: batch});
    };

    // if invalid date entered, app crashes
    // TODO add invalid date input handling, possibly by showing API error message
    // TODO move to API.js
    async createNewClicked() {
        const response = await Api.postBatches(this.props.token, this.state.editedBatch);
        // TODO add error handling, especially when batch_no exist (code 400)
        if (response) {
            this.props.addBatch(response)
        }
    };

    // if invalid date entered, app crashes
    // TODO add invalid date input handling, possibly by showing API error message
    // TODO move to API.js
    updateClicked = () => {
        fetch(`${process.env.REACT_APP_API_URL}/batches/${this.props.batch.id}/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${this.props.token}`
            },
            body: JSON.stringify(this.state.editedBatch)
        }).then( response => response.json())
            .then( data => this.props.batchUpdated(data))
            .catch( error => console.log(error))
    };

    render() {
        // TODO add more checks later
        // const isDisabled = this.state.editedBatch.batch_no.length === 0;

        return (
            <React.Fragment>
                <Form inverted>
                    <label>Batch Number</label>
                    <Form.Input
                        fluid
                        placeholder='Enter batch number..'
                        name='batch_no'
                        value={this.state.editedBatch.batch_no}
                        onChange={this.handleChange}
                    />
                    <label>Production</label>
                    <Dropdown
                        placeholder='Select Production'
                        name='production'
                        value={this.state.editedBatch.production}
                        onChange={this.handleChange}
                        fluid
                        openOnFocus
                        selection
                        options={
                            this.props.products.map( product => {
                                return (
                                    {key: product.id, text: product.name_lt, value: product.id}
                                )
                            })
                        }
                    />
                    <label>Batch date</label>
                    <Form.Input
                        placeholder="YYYY-MM-DD"
                        name='batch_date'
                        value={this.props.batch.batch_date}
                        onChange={this.handleChange}
                    />

                    { !this.state.editedBatch.id ? (
                        <Button inverted color='green' onClick={this.createNewClicked}>Create new</Button> )
                        : (
                        <Button inverted color='yellow' onClick={this.updateClicked}>Update</Button> )
                    }
                    <Button inverted floated='right' color="red" onClick={this.cancelClicked}>Cancel</Button>
                </Form>
            </React.Fragment>
        )
    }

}


export default BatchForm;