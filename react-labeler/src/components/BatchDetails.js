import React from "react";
import { Button, Icon, Item} from "semantic-ui-react";
import GeneratePDF from '../services/Pdf'

class BatchDetails extends React.Component {

    removeClicked = (batch) => {
        this.props.deleteBatch(batch)
    };

    editClicked = (batch) => {
        this.props.editBatch(batch)
    };

    cloneClicked = (batch) => {
        this.props.cloneBatch(batch)
    };

    render() {
        const batch = this.props.batch;
        return (
            <Item>
                {batch ? (
                    <Item.Content>
                        <Item.Header>
                            <h2>{batch.batch_no}</h2>
                        </Item.Header>
                        <Item.Description>
                            <p/>
                            <h3>{batch.production_info.name_lt}
                            <br/>
                            {batch.production_info.name_en}</h3>
                            <h4>{batch.batch_date}</h4>
                        </Item.Description>
                        <Item.Extra>
                            <br/>
                            <div>
                                <h5>Get label file:</h5>
                                <GeneratePDF
                                    batch={batch}
                                    company={this.props.company}
                                    key={batch.batch_no}
                                >
                                </GeneratePDF>
                            </div>
                            <br/>
                            <p>
                                <Button
                                    inverted
                                    size='tiny'
                                    onClick={() => this.cloneClicked(this.props.batch)}
                                >
                                    CLONE <Icon name='clone'/>
                                </Button>
                                <Button
                                    inverted
                                    size='tiny'
                                    onClick={() => this.editClicked(this.props.batch)}
                                >
                                    EDIT <Icon name='edit'/>
                                </Button>
                                <Button
                                    inverted color='red'
                                    size='tiny'
                                    floated='right'
                                    onClick={() => this.removeClicked(this.props.batch)}
                                >
                                    REMOVE <Icon name='trash'/>
                                </Button>
                            </p>
                        </Item.Extra>
                    </Item.Content>
                    ) : ''
                }
            </Item>
        );
    }

}

export default BatchDetails;