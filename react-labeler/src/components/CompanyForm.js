import React from "react";
import {Button, Container, Grid, Header, Input} from "semantic-ui-react";
import Api from "../services/Api";

class CompanyForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            company: this.props.company
        };
        this.handleCreate = this.handleCreate.bind(this)
        this.handleUpdate = this.handleUpdate.bind(this)
    }

    handleChange = (e, { value, name }) => {
        let company = this.state.company;
        company[name] = value;
        this.setState({company: company});
    };

    handleFileFieldChange = (e, {name}) => {
        let company = this.state.company;
        company[name] = e.target.files[0];
        this.setState({company: company})
    };

    async handleCreate() {
        const data = new FormData();
        for (const [name, value] of Object.entries(this.state.company)) {
           data.append(name, value)
        }
        const response = await Api.postCompany(this.props.token, data);
        if (response.id) {
            this.props.addNewCompany(response);
            this.props.selectCompany(response);
            this.props.cancelForm();
            // TODO refresh view with details of newly created company
        } // TODO add error handling
    };

    async handleUpdate() {
        const data = new FormData();
        for (const [name, value] of Object.entries(this.state.company)) {
            data.append(name, value)
        }
        const response = await Api.putCompany(this.props.token, this.state.company.id, data);
        // TODO Fix bug. returns error: 400 Bad request
        if (response.id) {
            this.props.updateCompanyInfo(response)
            this.props.selectCompany(response);
            this.props.cancelForm();
        } // TODO add error handling
    };

    render() {
        return (
            <Container>
                <Header as='h3' textAlign='center'>
                    {this.state.company.id ? (
                        'Update company details'
                    ) :
                        'Create new company'
                    }
                </Header>
                <Grid container verticalAlign='middle'>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Company Name</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <Input
                                placeholder='Enter company name..'
                                name='name'
                                value={this.state.company.name}
                                onChange={this.handleChange}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Company Code</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <Input
                                placeholder='Enter company code..'
                                name='reg_code'
                                value={this.state.company.reg_code}
                                onChange={this.handleChange}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Address</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <Input
                                placeholder='Enter company address..'
                                name='address'
                                value={this.state.company.address}
                                onChange={this.handleChange}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Certification body code</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <Input
                                placeholder='Enter code of certification body'
                                name='cert_body_code'
                                value={this.state.company.cert_body_code}
                                onChange={this.handleChange}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Country of origin</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <Input
                                placeholder='Enter country of origin'
                                name='cert_body_country'
                                value={this.state.company.cert_body_country}
                                onChange={this.handleChange}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Company logo</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <Input
                                type='file'
                                name='company_logo'
                                onChange={this.handleFileFieldChange}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Organic logo</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <Input
                                type='file'
                                name='organic_logo'
                                onChange={this.handleFileFieldChange}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            {!this.state.company.id ? (
                                <Button
                                    inverted
                                    color='green'
                                    onClick={this.handleCreate}>
                                    Create
                                </Button>
                            ) :
                                <Button
                                    color='green'
                                    onClick={this.handleUpdate}>
                                    Update
                                </Button>
                            }
                            <Button
                                color="red"
                                onClick={this.props.cancelForm}>
                                Cancel
                            </Button>

                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        )
    }
}

export default CompanyForm;