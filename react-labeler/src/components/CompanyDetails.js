import React from "react";
import {Container, Header, Grid, Image, Icon, Button} from "semantic-ui-react";

class CompanyDetails extends React.Component {

    render() {
        const selectedCompany = this.props.selectedCompany;
        return (
            <Container>
                <Grid container verticalAlign='middle'>
                    <Grid.Row>
                        <Grid.Column>
                            <Button
                                color='grey'
                                floated='right'
                                size='tiny'
                                disabled // Need to fix edit functionality first
                                onClick={() => this.props.editCompany(selectedCompany)}
                            >
                                EDIT <Icon name='edit'/>
                            </Button>
                            <Button
                                color='red'
                                floated='right'
                                size='tiny'
                                onClick={() => this.props.handleRemove(selectedCompany)}
                            >
                                REMOVE <Icon name='trash'/>
                            </Button>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column textAlign='right'>
                            <Header as='h3' textAlign='center'>{selectedCompany.name}</Header>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Company Code</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <p>{selectedCompany.reg_code}</p>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Address</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <p>{selectedCompany.address}</p>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Certification body code</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <p>{selectedCompany.cert_body_code}</p>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Country of certification body</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <p>{selectedCompany.cert_body_country}</p>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Company logo</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <Image src={selectedCompany.company_logo}/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign='right'>
                            <p>Organic logo</p>
                        </Grid.Column>
                        <Grid.Column textAlign='left'>
                            <Image src={selectedCompany.organic_logo}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        )
    }
}

export default CompanyDetails;