## Usage with docker

### BACKEND Labeler_backend

#### Build image:
```shell script
docker build -t registry.gitlab.com/kotlabs/django-api-react-labeler/labeler_backend:latest-slim-buster Labeler_backend/
```

### Development 
#### Run image for development with volumes mounted:
```shell script
docker run -p 8000:8000 -v $(pwd)/Labeler_backend/:/usr/app/web --entrypoint /usr/local/bin/python registry.gitlab.com/kotlabs/django-api-react-labeler/labeler_backend:latest-slim-buster ./manage.py runserver 0.0.0.0:8000
```

### Production
#### Required environment variables
DJANGO_ENV=prod
DJANGO_DB=postgres
POSTGRES_USER_FILE
POSTGRES_PASSWORD_FILE
POSTGRES_HOST_FILE
POSTGRES_PORT_FILE
DJANGO_SECRET_KEY_FILE
DJANGO_ALLOWED_HOSTS_FILE
DJANGO_CORS_ORIGIN_WHITELIST_FILE

### FRONTEND react-labeler
### Development 

#### Build development image
```shell script
docker build --target development -t registry.gitlab.com/kotlabs/django-api-react-labeler/react-labeler:dev react-labeler/
```

#### Build production image
```shell script
docker build -t registry.gitlab.com/kotlabs/django-api-react-labeler/react-labeler:latest react-labeler/
```

#### Run react-labeler container (backend API must be running) for development
```shell script
docker run -it --rm -v ${PWD}/react-labeler:/app -v /app/node_modules -p 3000:3000 -e CHOKIDAR_USEPOLLING=true registry.gitlab.com/kotlabs/django-api-react-labeler/react-labeler:dev
```

### Production
#### Required environment variables
REACT_APP_API_URL

## Using with docker swarm

Run:  
```shell script
docker stack deploy -c labeler-stack.localdev.yaml labeler
```
